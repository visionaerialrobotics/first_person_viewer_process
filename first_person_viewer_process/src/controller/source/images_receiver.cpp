/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  imagesReceiver
  Launchs a ROS node to subscribe and process images.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/images_receiver.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

ImagesReceiver::ImagesReceiver()
{
  subscriptions_complete = false;
  surface_inspection = false;
  hud_activated = true;
  receiving_drone_images = false;
}

ImagesReceiver::~ImagesReceiver()
{
}

void ImagesReceiver::openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace)
{
  if (!nodeHandle.getParam("drone_console_interface_sensor_bottom_camera",
                           drone_console_interface_sensor_bottom_camera))
    drone_console_interface_sensor_bottom_camera = "camera/bottom/image_raw";

  if (!nodeHandle.getParam("drone_console_interface_sensor_front_camera", drone_console_interface_sensor_front_camera))
    drone_console_interface_sensor_front_camera = "camera/front/image_raw";

  if (!nodeHandle.getParam("surface_inspection_topic", surface_inspection_topic))
    drone_console_interface_sensor_front_camera = "surface_inspection_image";

  if (!nodeHandle.getParam("drone_console_interface_sensor_overlay_camera_big",
                           drone_console_interface_sensor_overlay_camera_big))
    drone_console_interface_sensor_overlay_camera_big = "camera/overlay/image_raw/big";
  if (!nodeHandle.getParam("drone_console_interface_sensor_overlay_camera_big",
                           drone_console_interface_sensor_overlay_camera_small))
    drone_console_interface_sensor_overlay_camera_small = "camera/overlay/image_raw/small";

  // Topic communications
  image_transport::ImageTransport it_(nodeHandle);

  image_overlay_big_ = it_.subscribe("/" + rosnamespace + "/" + drone_console_interface_sensor_overlay_camera_big, 1,
                                     &ImagesReceiver::imagesOverlayBigReceptionCallback, this);
  image_overlay_small_ = it_.subscribe("/" + rosnamespace + "/" + drone_console_interface_sensor_overlay_camera_small,
                                       1, &ImagesReceiver::imagesOverlaySmallReceptionCallback, this);

  image_bottom_sub_ = it_.subscribe("/" + rosnamespace + "/" + drone_console_interface_sensor_bottom_camera, 1,
                                    &ImagesReceiver::imagesBottomReceptionCallback, this);
  image_front_sub_ = it_.subscribe("/" + rosnamespace + "/" + drone_console_interface_sensor_front_camera, 1,
                                   &ImagesReceiver::imagesFrontReceptionCallback, this);
  image_surface_inspection_sub_ = it_.subscribe("/" + rosnamespace + "/" + surface_inspection_topic, 1,
                                                &ImagesReceiver::imagesSurfaceInspectionCallback, this);

  subscriptions_complete = true;
}

bool ImagesReceiver::ready()
{
  if (!subscriptions_complete)
    return false;
  return true;  // Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}

QImage ImagesReceiver::cvtCvMat2QImage(const cv::Mat& image)
{
  QImage qtemp;
  if (!image.empty() && image.depth() == CV_8U)
  {
    const unsigned char* data = image.data;
    qtemp = QImage(image.cols, image.rows, QImage::Format_RGB32);
    for (int y = 0; y < image.rows; ++y, data += image.cols * image.elemSize())
    {
      for (int x = 0; x < image.cols; ++x)
      {
        QRgb* p = ((QRgb*)qtemp.scanLine(y)) + x;
        *p = qRgb(data[x * image.channels() + 2], data[x * image.channels() + 1], data[x * image.channels()]);
      }
    }
  }
  else if (!image.empty() && image.depth() != CV_8U)
  {
    printf("Wrong image format\n");
  }
  return qtemp;
}

void ImagesReceiver::imagesBottomReceptionCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_bottom_image;
  try
  {
    cv_bottom_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  px = QPixmap::fromImage(cvtCvMat2QImage(cv_bottom_image->image));

  Q_EMIT Update_Image(&px, 2);
}

void ImagesReceiver::imagesFrontReceptionCallback(const sensor_msgs::ImageConstPtr& msg)
{
  receiving_drone_images = true;
  if (!surface_inspection)
  {
    // if(!hud_activated){
    cv_bridge::CvImagePtr cv_front_image;
    try
    {
      cv_front_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    px = QPixmap::fromImage(cvtCvMat2QImage(cv_front_image->image));

    Q_EMIT Update_Image(&px, 1);
  }
  // }
}

void ImagesReceiver::imagesSurfaceInspectionCallback(const sensor_msgs::ImageConstPtr& msg)
{
  if (surface_inspection)
  {
    cv_bridge::CvImagePtr surface_image;
    try
    {
      surface_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    px = QPixmap::fromImage(cvtCvMat2QImage(surface_image->image));

    Q_EMIT Update_Image(&px, 1);
  }
}

void ImagesReceiver::setInspectionMode(bool b)
{
  surface_inspection = b;
}

void ImagesReceiver::imagesOverlaySmallReceptionCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_overlay_small;
  try
  {
    cv_overlay_small = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  px_small = QPixmap::fromImage(cvtCvMat2QImage(cv_overlay_small->image));

  Q_EMIT Update_Image_Overlay_Small(&px_small);
}

void ImagesReceiver::imagesOverlayBigReceptionCallback(const sensor_msgs::ImageConstPtr& msg)
{
  // if(hud_activated){

  cv_bridge::CvImagePtr cv_overlay_big;
  try
  {
    cv_overlay_big = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  px_big = QPixmap::fromImage(cvtCvMat2QImage(cv_overlay_big->image));

  Q_EMIT Update_Image_Overlay_Big(&px_big);
  // Q_EMIT Update_Image(&px_big,1);
  // }
}

void ImagesReceiver::changeHudActivated()
{
  // if(receiving_drone_images)
  hud_activated = !hud_activated;
}

bool ImagesReceiver::getHudActivated()
{
  return hud_activated;
}
