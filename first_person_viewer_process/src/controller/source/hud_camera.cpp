/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  Camera view - show the camera views displayed in a grid
  @author  Daniel Rabasco
  @date    06-2015
  @version 1.0
*/

/*****************************************************************************
 ** Includes
 *****************************************************************************/
#include "../include/hud_camera.h"
/*****************************************************************************
 ** Implementation
 *****************************************************************************/

HUDCamera::HUDCamera(FirstView* first_person, ImagesReceiver* img_receiver, QWidget* parent)
  : QWidget(parent), ui(new Ui::HUDCamera)
{
  ui->setupUi(this);

  image_receiver = img_receiver;
  first_person_view = first_person;

  overlay_big = false;
  this->resize(500, 395);
  ui->toolButton->setIcon(QIcon(":/img/img/zoom_icon.png"));

  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  setWindowIcon(QIcon(":/img/img/first_person_viewer.png"));

  connect(image_receiver, SIGNAL(Update_Image_Overlay_Small(const QPixmap*)), this,
          SLOT(updateImageOverlaySmall(const QPixmap*)));
  connect(image_receiver, SIGNAL(Update_Image_Overlay_Big(const QPixmap*)), this,
          SLOT(updateImageOverlayBig(const QPixmap*)));

  // Layout
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("FIRST_PERSON_VIEWER.height");
  int width = root.get<int>("FIRST_PERSON_VIEWER.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("FIRST_PERSON_VIEWER.position.little.x"), y0 + root.get<int>("FIRST_PERSON_VIEWER.position.little.y"));
  cont = 0;

  // Settings connections
  setUp();
}

void HUDCamera::setUp()
{
  n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");

  n.param<std::string>("window_event_topic", window_event_topic, "window_event");

  // Subscribers
  window_event_sub =
      n.subscribe("/" + drone_id_namespace + "/" + window_event_topic, 10, &HUDCamera::windowOpenCallback, this);

  // Publishers
   window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + drone_id_namespace + "/" + window_event_topic, 1, true);
}

void HUDCamera::updateImageOverlaySmall(const QPixmap* image)
{
  if (!overlay_big)
  {
    pixmap = *image;
    if (!image->isNull())
    {
      ui->image->setScaledContents(true);
      ui->image->setPixmap(pixmap);
    }
  }
}

void HUDCamera::updateImageOverlayBig(const QPixmap* image)
{
  if (overlay_big)
  {
    pixmap = *image;
    if (!image->isNull())
    {
      ui->image->setPixmap(pixmap.scaled(ui->image->width(), ui->image->height(), Qt::IgnoreAspectRatio));
    }
  }
}

void HUDCamera::on_toolButton_clicked()
{
  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;

  overlay_big = !overlay_big;

  if (overlay_big)
  {
    int height = root.get<int>("FIRST_PERSON_VIEWER.heightBig");
    int width = root.get<int>("FIRST_PERSON_VIEWER.widthBig");
    this->resize(width, height);

    if (cont == 0)
    {  // the first time we click it counts header height

      this->move(x0 + root.get<int>("FIRST_PERSON_VIEWER.position.big.x"), y0 + root.get<int>("FIRST_PERSON_VIEWER.position.big.y"));
      cont++;
    }
    else
    {
      this->move(x0 + root.get<int>("FIRST_PERSON_VIEWER.position.big.x"), y0 + root.get<int>("FIRST_PERSON_VIEWER.position.big.y"));
    }
  }
  else
  {
    int height = root.get<int>("FIRST_PERSON_VIEWER.height");
    int width = root.get<int>("FIRST_PERSON_VIEWER.width");

    this->resize(width, height);
    this->move(x0 + root.get<int>("FIRST_PERSON_VIEWER.position.little.x"), y0 + root.get<int>("FIRST_PERSON_VIEWER.position.little.y"));
  }
}

void HUDCamera::setOverlayBig(bool overlay)
{
  overlay_big = overlay;
}

HUDCamera::~HUDCamera()
{
  delete ui;
}
void HUDCamera::closeEvent(QCloseEvent* event)
{
  window_event_msg.window = aerostack_msgs::WindowEvent::FIRST_PERSON_VIEWER;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
}
void HUDCamera::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void HUDCamera::windowOpenCallback(const aerostack_msgs::WindowEvent& msg)
{
  

  if (msg.window == aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL  ||
      msg.window == aerostack_msgs::WindowEvent::PYTHON_MISSION_INTERPRETER  ||
      msg.window == aerostack_msgs::WindowEvent::PYTHON_CONTROL ||
      msg.window == aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER )
  {
    window_event_msg.window = aerostack_msgs::WindowEvent::FIRST_PERSON_VIEWER;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
    killMe();
  }

  if (msg.window == aerostack_msgs::WindowEvent::INTEGRATED_VIEWER && msg.event == aerostack_msgs::WindowEvent::MINIMIZE)
    showMinimized();
}
