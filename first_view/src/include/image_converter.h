
/********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef IMAGE_CONVERTER_H
#define IMAGE_CONVERTER_H

#include <ros/ros.h>

/*Messages*/
#include "geometry_msgs/Pose.h"
#include "droneMsgsROS/battery.h"
#include "geometry_msgs/TwistStamped.h"
#include <sensor_msgs/image_encodings.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/version.hpp>

#include <string>
#include <iostream>
#include <math.h>
#include <chrono>
#include <tf/tf.h>
#include <time.h>
#include <Eigen/Geometry>
#include <angles/angles.h>

// Mind 30 fps
// cv::Scalar are color objects.
// Mind they are defined in BGRA order and alpha should be 255.
#define FONT cv::FONT_HERSHEY_DUPLEX
#define FONT_SCALE 0.45
#define SMALL_FONT_SCALE 0.75  // Not recommended more than 0.75
#define TEXT_THICKNESS 1       // Integer
#define LINE_TYPE CV_AA        // Better looking line
//#define BORDER_TYPE CV_AA//If applied border. Unused
#define DARK_COLOR cv::Scalar(0, 0, 0, 255)        // Black
#define TEXT_COLOR cv::Scalar(255, 255, 255, 255)  // White
#define FPS_CAP 0                                  // 0=no cap  (max 30) Unused
#define LEFT_PANEL_POINT cv::Point(10, 30)         // 10 pixels left, 30 pixels up
#define HORIZON_LINE_SCALE 1                       // Recommeded values 1..2
#define POV_ANGLE 15                               // Pov angle for pitch in horizon line == 42 degrees/2
#define PITCH_SCALE 1                              // Pitch scale represented in pitch. Should be one.
#define PITCH_REFRESH_RATE 20                      // Number of frames for pitch transition
#define ROLL_REFRESH_RATE 20                       // Same as the pitch pitch refresh rate
#define BIG 0
#define SMALL 1

#define PITCH_NO 5                  // Number of data in the pitch indicator
#define BRIGHTNESS_LEVEL 255 * 0.6  // 0 Black 255 White

static const std::string OPENCV_WINDOW = "Image window";  // For debug with cv::imshow

class ImageConverter
{
public:
  // Constructor & Destructor
  ImageConverter();
  ~ImageConverter();

public:
  void CheckUnderlay();
  void CheckOverlay(cv::Mat& overlay, int im_no);
  void poseInfoCb(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void spdInfoCb(const geometry_msgs::TwistStamped::ConstPtr& msg);
  void battInfoCb(const droneMsgsROS::battery::ConstPtr& msg);
  void imageOverlayCb(const sensor_msgs::ImageConstPtr& msg, const image_transport::Publisher& image_pub_small_,
                      const image_transport::Publisher& image_pub_big_);
  void imageOverlayCbBlack(const image_transport::Publisher& image_pub_small_,
                           const image_transport::Publisher& image_pub_big_);
  void secondOperations();
  void checkBrightness();
  void putText(cv::Mat& img, const std::string& text, cv::Point point, double text_size, cv::Scalar color_,
               int thickness);
  void overlayImage(cv::Mat* src, cv::Mat* overlay, const cv::Point& location);
  std::vector<int> linspace(int min, int max, int n);
  void generateOverlay(cv::Mat& overlay);
  void refreshVariables(cv::Mat& overlay, int im_no);
  void DrawCenterPannel(cv::Mat& overlay, int im_no);
  void DrawLeftPannel(cv::Mat& overlay);
  void DrawRightPannel(cv::Mat& overlay);
  void DrawRollIndicator(cv::Mat& overlay, int im_no);
  void DrawHorizonLine(cv::Mat& overlay, int im_no);
  void DrawYawIndicator(cv::Mat& overlay, int im_no);
  double DegToRad(double deg);
  double RadToDeg(double rad);
  std::string doToStr(double a, int n_dec);
  cv::Point rotatePoint(cv::Point center, cv::Point point, double angle);
  void toEulerAngle(const Eigen::Quaternionf& q, float& roll, float& pitch, float& yaw);

  time_t start, end;
  int frame, real_fps, battery_status, measures, seconds, diff_time, minutes, published_frame, radius, height,
      home_dist, brightness_change, dark_mode, icon_load, pitch_change, pitch_change_phase, tmp_pitch, roll_change,
      roll_change_phase, tmp_roll;
  float yaw, pitch, roll;
  size_t total_size;
  std::clock_t timer_start;
  double duration, total_time, frame_time, pos_x, pos_y, pos_z, spd, initial_angle, final_angle, pitch_offset;
  static double font_scale;

  std::vector<int> path, pitch_transform, roll_transform;

  typedef std::chrono::high_resolution_clock Clock;
  cv::Size textSize;
  static cv::Scalar color;
  cv::Point start_left, start_right, draw_left, draw_right, ell_center, ell_init;  // HUD position points
  cv::Mat battery, position;
  bool hum, yeah;  // Testing variables

private:
  /*ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_big_,image_pub_small_;
  ros::Subscriber pose_sub_,batt_sub_,spd_sub_;*/
  cv::Mat fixed_overlay, image_big, image_small, backup;
  cv_bridge::CvImagePtr cv_ptr;
};

#endif
